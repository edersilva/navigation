import React from 'react';
import { View, Text, Button } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';



import {
    createMaterialTopTabNavigator,
    createAppContainer,
  } from 'react-navigation';


  class DetailsScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>Details!</Text>
        </View>
      );
    }
  }
  
  class HomeScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          {/* other code from before here */}
          <Button
            title="Go to Details"
            onPress={() => this.props.navigation.navigate('Details')}
          />
        </View>
      );
    }
  }
  
  class SettingsScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          {/* other code from before here */}
          <Button
            title="Go to Details"
            onPress={() => this.props.navigation.navigate('Details')}
          />
        </View>
      );
    }
  }
  
  
  const Navigator = createMaterialTopTabNavigator(
    {
      Home: { 
        screen: HomeScreen,
        navigationOptions:{
            tabBarLabel: 'Home',
            tabBarIcon: ({tintColor}) => (
                <Icon name="home" color={tintColor} size={24} />
            )
        } 
    },
      Settings: { 
        screen: SettingsScreen,
        navigationOptions:{
            tabBarLabel: 'Settings',
            tabBarIcon: ({tintColor}) => (
                <Icon name="dashboard" color={tintColor} size={24} />
            )
        } 
      }
    },
    {
      /* Other configuration remains unchanged */
      tabBarOptions:{
        activeTintColor: '#ff9900',
        inactiveTintColor: '#ccc',
        showIcon: true,
      }
    }
  );


  const MenuTopNavigator = createAppContainer(Navigator);

  export default MenuTopNavigator